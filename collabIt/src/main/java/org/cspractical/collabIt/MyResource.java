package org.cspractical.collabIt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.cspractical.dal.MongoDalLayer;

import com.mongodb.util.JSON;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource {

	MongoDalLayer mongoDalLayer = new MongoDalLayer();

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to
	 * the client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@Path("/persistData")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String persistData(String inputMap) {

		System.out.println(inputMap);
		HashMap parsedMap = (HashMap) JSON.parse(inputMap);
		System.out.println(parsedMap);
		String name = (String) parsedMap.get("Name");
		mongoDalLayer.upsert(parsedMap);
		return JSON.serialize(name);

	}

	@Path("/retrieveData")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String retrieveData(String inputMap) {

		System.out.println(inputMap);
		HashMap parsedMap = (HashMap) JSON.parse(inputMap);
		System.out.println(parsedMap);
		String id = (String) parsedMap.get("_id");
		List<Map> resultList = mongoDalLayer.find(id);
		if (resultList.size() == 1) {
			Map map = resultList.get(0);
			return JSON.serialize(map);
		}
		return JSON.serialize("Something is worng with app! Please refresh");

	}

	@Path("/addCollaborator")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addCollaborator(String inputMap) {

		System.out.println(inputMap);
		HashMap parsedMap = (HashMap) JSON.parse(inputMap);
		System.out.println(parsedMap);
		String name = (String) parsedMap.get("Name");
		String id = (String) parsedMap.get("_id");
		boolean flag = mongoDalLayer.addToSet(name, id);
		return JSON.serialize(flag);

	}
}
