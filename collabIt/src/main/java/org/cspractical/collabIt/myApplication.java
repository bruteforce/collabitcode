package org.cspractical.collabIt;

import org.glassfish.jersey.server.ResourceConfig;

public class myApplication extends ResourceConfig {
	public myApplication() {
		register(CORSResponseFilter.class);
	}

}
