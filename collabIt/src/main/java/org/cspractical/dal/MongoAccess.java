package org.cspractical.dal;

import java.net.UnknownHostException;

import com.mongodb.MongoClient;

public class MongoAccess {
	private static volatile MongoClient mongoClient;

	private MongoAccess() {

	}

	public static synchronized MongoClient getInstance() {

		if (mongoClient == null) {

			try {
				mongoClient = new MongoClient("localhost", 27017);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			return mongoClient;

		} else {
			return mongoClient;
		}

	}

}
