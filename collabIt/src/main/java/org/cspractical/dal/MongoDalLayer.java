package org.cspractical.dal;

import java.lang.invoke.WrongMethodTypeException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.BSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;

public class MongoDalLayer {
	MongoClient mongoClient;
	DB dataBase;
	DBCollection coll;

	public MongoDalLayer() {
		mongoClient = MongoAccess.getInstance();
		dataBase = mongoClient.getDB("collabit_db");
		coll = dataBase.getCollection("documents_coll");

	}

	public boolean upsert(Map map) {

	//	BasicDBObject dbObject = getBasicDBObjectMap(map);
		String id = (String) map.get("_id");
		String name = (String) map.get("Name");
		Integer position = Integer.parseInt(map.get("Position").toString());
		String data = (String) map.get("Data");

		BasicDBObject searchQuery = new BasicDBObject("_id", id);

		BasicDBObject setQuery = new BasicDBObject("$set",
				new BasicDBObject(name + "Pos", position).append("Data", data));
		
		System.out.println(setQuery);

		WriteResult result = coll.update(searchQuery, setQuery);
		if (result.getN() == 0) {
			return false;
		} else {
			return true;
		}
	}

	private BasicDBObject getBasicDBObjectMap(Map map) {
		BasicDBObject dbObject = new BasicDBObject();
		Set<String> keySet = map.keySet();
		for (String key : keySet) {
			dbObject.append(key, map.get(key));
		}
		return dbObject;
	}

	public List<Map> find(String id) {
		BasicDBObject dbObject = new BasicDBObject("_id", id);
		DBCursor dbCursor = coll.find(dbObject);
		List<Map> result = getResult(dbCursor);
		return result;
	}

	private List<Map> getResult(DBCursor dbCursor) {
		List<Map> resultList = new ArrayList<Map>();

		while (dbCursor.hasNext()) {
			Map innerMap = dbCursor.next().toMap();
			resultList.add(innerMap);
		}
		return resultList;
	}

	public boolean addToSet(String name, String id) {
		BasicDBObject findQuery = new BasicDBObject("_id", id);
		BasicDBObject updateQuery = new BasicDBObject("$addToSet", new BasicDBObject("collaborators", name));
		WriteResult result = coll.update(findQuery, updateQuery, true, false, WriteConcern.MAJORITY);
		if (result.getN() == 0) {
			return false;
		} else {
			return true;
		}
	}

}
