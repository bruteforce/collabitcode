var name;
var params;
$(document).ready(function() {

    params = getSearchParameters();
    console.log(params);
    setName();
    console.log(name);
	$("#yourName").html(name);
    addCollaborator();
	
	
});


function setName() {
    var person = prompt("Please enter your name", "");

    if (person != null) {
        name = person;
    }
};




function getSearchParameters() {
    var prmstr = window.location.search.substr(1);
    return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray(prmstr) {
    var params = {};
    var prmarr = prmstr.split("&");
    for (var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}


function persistData() {
    $(function() {
        var map = {};
        var data = $("#data").val();
        var id = params.id;
        console.log(data);
        map["Name"] = name;
        map["Data"] = data;
        map["_id"] = id;
		map["Position"]=doGetCaretPosition(document.getElementById('data'));
		

        $.ajax({
            type: 'POST',
            url: 'http://52.74.191.58:8080/collabIt/webapi/myresource/persistData',
            data: JSON.stringify(map),
            contentType: 'application/json',
            success: function(data) {
                console.log(data);
            }
        }).error(function() {
            alert('oops!,An error has occured,Try again!');
        });
    });
}

function addCollaborator() {
    $(function() {
        var map = {};
        map["Name"] = name;
		var id = params.id;
        map["_id"] = id;

        $.ajax({
            type: 'POST',
            url: 'http://52.74.191.58:8080/collabIt/webapi/myresource/addCollaborator',
            data: JSON.stringify(map),
            contentType: 'application/json',
            success: function(data) {
                console.log(data);
            }
        }).error(function() {
            alert('oops!,An error has occured,Try again!');
        });
    });
	retrieveData();
}


$( "#data" ).keyup(function() {
	persistData();
});

function doGetCaretPosition (ctrl) {
	var CaretPos = 0;	// IE Support
	if (document.selection) {
	ctrl.focus ();
		var Sel = document.selection.createRange ();
		Sel.moveStart ('character', -ctrl.value.length);
		CaretPos = Sel.text.length;
	}
	// Firefox support
	else if (ctrl.selectionStart || ctrl.selectionStart == '0')
		CaretPos = ctrl.selectionStart;
	return (CaretPos);
}
function setCaretPosition(ctrl, pos){
	if(ctrl.setSelectionRange)
	{
		ctrl.focus();
		ctrl.setSelectionRange(pos,pos);
	}
	else if (ctrl.createTextRange) {
		var range = ctrl.createTextRange();
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}
}
